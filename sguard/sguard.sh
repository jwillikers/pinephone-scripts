#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2021 JayVii, 2021 HazardChem
# SPDX-License-Identifier: GPL-3.0-or-later
# About this header: https://reuse.software


# Checks ----------------------------------------------------------------------

# is pacmd installed?
if [[ -z `whereis pacmd | awk '{ print $2 }'` ]]; then
    echo "pacmd is required for sguard to function properly."
    exit 1;
fi

# is gnome-session-inhibit installed?
if [[ -z `whereis gnome-session-inhibit | awk '{ print $2 }'` ]]; then
    echo "gnome-session-inhibit is required for sguard to function properly."
    exit 2;
fi

# Configuration ---------------------------------------------------------------
if [[ -z $DEBUG ]]; then
    DEBUG="false"
fi
if [[ -z $TIMEOUT ]]; then
    TIMEOUT=60
fi

# Functions -------------------------------------------------------------------

# check player state.
function check_player_state {
    if [[ -z `pacmd list sinks | grep -E "(s|S)tate: RUNNING$"` ]]; then
        echo false
    else
        echo true
    fi
}

# prevent sleep temporary (Timeout + 1 sec)
function prevent_sleep {
    gnome-session-inhibit --inhibit suspend \
                          --reason "Audio is playing..." \
                          sleep $(($TIMEOUT + 1))
}

# Execution -------------------------------------------------------------------

while :; do
    if [[ `check_player_state` == "true" ]]; then
        prevent_sleep &
        if [[ "$DEBUG" == "true" ]]; then
            echo "inhibit sleep for $TIMEOUT seconds"
        fi
    fi
    sleep $TIMEOUT # Zzzz...
done

# EOF sguard.sh
